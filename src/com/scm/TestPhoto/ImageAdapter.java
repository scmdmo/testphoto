package com.scm.TestPhoto;

import java.io.File;
import java.io.FilenameFilter;
import java.lang.ref.WeakReference;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class ImageAdapter extends BaseAdapter {
    private final Context context;
    private File album; 
    
    /* Imageviews dimension*/
    private final int width;
    private final int height;
    private final int padding;
    
    public ImageAdapter(Context c) {    	
    	context = c;    	
    	final DisplayMetrics metrics=context.getResources().getDisplayMetrics();
    	width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, metrics);
        height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 100, metrics);
        padding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 3, metrics);
    }   

	public int getCount() {
		// si se pueden leer las imagenes devolvemos el numero que haya
		if (canreadImages()){			
			FilenameFilter filter= new FilenameFilter() {
			    public boolean accept(File dir, String name){
			        return (name.endsWith(".jpg")||name.endsWith(".jpeg")); 
			    }
			};			
	    	int numpic=album.listFiles(filter).length;
	    	//EXCEDENT: sobran estas lineas de depuracion
	    	//Log.d("IMAGEGRID","getCount="+String.valueOf(numpic));
//			int i=0;
//			for (File pic:album.listFiles(filter))
//				Log.d("IMAGEGRID","PIC"+(i++)+" "+pic.getName());

	        return numpic;
		}
		//si no se pueden leer, asumimos que no hay ninguna
		return 0;
    }
    
    // no lo vamos a usar asi que lo dejamos vac�o
    public Object getItem(int position) {    
        return null;
    }
    
    // no lo vamos a usar asi que lo dejamos vac�o
    public long getItemId(int position) {    	
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {    	
        Log.d("IMAGEGRID", "in getView for position " + position + ", convertView is "
                + ((convertView == null) ? "null" : "being recycled"));
        ImageView imageView;
        if (convertView == null) {  // if it's not recycled, initialize some attributes
            imageView = new ImageView(context);            
            imageView.setLayoutParams(new GridView.LayoutParams(width, height));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(0, padding, 0, padding);
        } else {
            imageView = (ImageView) convertView;
        }       
        
        loadImage(imageView,position);        
        return imageView;
    }
    
    // comprueba si el medio esta montado y existe el directorio del album
    private boolean canreadImages(){
    	String state = Environment.getExternalStorageState();
		if(Environment.MEDIA_MOUNTED.equals(state)){			
			File aux = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);		
			File album = new File(aux,context.getString(R.string.album_name));
			if (album.isDirectory() && album.canRead()){
				this.album=album;
				return true;
			}						
		}
		this.album=null;
		return false;
    }    
    
    private void loadImage(ImageView imageView,int position){
    	File[] files=album.listFiles();
    	if (cancelPotentialWork(files[position],imageView)){	    	
	    	final LoadPhotoTask asyncload = new LoadPhotoTask(imageView);
	    	final Bitmap waitbitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.placeholder); 
	    	final AsyncDrawable asyncDrawable = new AsyncDrawable(context.getResources(),waitbitmap,asyncload); 
	//    	asyncload.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, files[position]);    	
	    	asyncload.execute(files[position]);
	    	imageView.setImageDrawable(asyncDrawable);//selecionamos una imagen temporal
    	}
    }
    
    public static boolean cancelPotentialWork(File file, ImageView imageView) {
        final LoadPhotoTask loadPhotoTask = getLoadPhotoTaskRunning(imageView);
        if (loadPhotoTask != null) {
            final File taskFile = loadPhotoTask.file;
            // Si el taskFile de la asynctask no esta asignado o difiere del nuevo file
            if (taskFile == null || !taskFile.equals(file)) {
                // Cancelar la tarea previa
            	loadPhotoTask.cancel(true);
            } else {
            	// El trabajo en progreso es el mismo
                return false;
            }
        }
        // No hay tareas asociadas con el ImageView o la tarea existente fue cancelada
        return true;
    }
    
    private static LoadPhotoTask getLoadPhotoTaskRunning(ImageView imageView) {
    	   if (imageView != null) {
    	       final Drawable drawable = imageView.getDrawable();
    	       if (drawable instanceof AsyncDrawable) {
    	           final AsyncDrawable asyncDrawable = (AsyncDrawable) drawable;
    	           return asyncDrawable.getLoadPhotoTask();
    	       }
    	    }
    	    return null;
    }
    
    
    /*Funciones para la Carga eficiente de los bitmaps*/
    //usan tecnicas de subsampling (calculateInSampleSize) y calculo del tama�o de carga antes de realizarla ()
    private static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
	    // Raw height and width of image
	    final int height = options.outHeight;
	    final int width = options.outWidth;
	    int inSampleSize = 1;
	
	    if (height > reqHeight || width > reqWidth) {
	
	        final int halfHeight = height / 2;
	        final int halfWidth = width / 2;
	
	        // Calculate the largest inSampleSize value that is a power of 2 and keeps both
	        // height and width larger than the requested height and width.
	        while ((halfHeight / inSampleSize) > reqHeight
	                && (halfWidth / inSampleSize) > reqWidth) {
	            inSampleSize *= 2;
	        }
	    }
	
	    return inSampleSize;
    }
    
    private static Bitmap decodeSampledBitmapFromResource(String pathName,
            int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true; //selecionamos la decodificacion que solo calcula los parametros de la imagen en la salidas de options
        BitmapFactory.decodeFile(pathName, options);  //obtenemos lo parametros de la imagen en options

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight); //calculamos el subsamplig para la imagen (reducion de tama�o)

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;//selecionamos la decodificacion normal
        return BitmapFactory.decodeFile(pathName, options); //obtenemos la imagen decodificada en un bitmap segun las opcione de entrada que establecimos
    }

       
   /**
    * Clase para guardar la refecencia al imageView que se esta cargando dentro de una Asyntask
    * LoadPhotoTask para 
    * @author Domin
    *
    */
    static class AsyncDrawable extends BitmapDrawable {
    	//desenlazamos la variable de que la tarea exista, 
    	//(permitir que el garbage collector libere la memoria si acabo o se cancelo)
        private final WeakReference<LoadPhotoTask> loadPhotoTaskReference;
        
        public AsyncDrawable(Resources res, Bitmap bitmap,LoadPhotoTask loadPhotoTask) {
            super(res, bitmap);
            loadPhotoTaskReference = new WeakReference<LoadPhotoTask>(loadPhotoTask);
        }

        public LoadPhotoTask getLoadPhotoTask() {
            return loadPhotoTaskReference.get();
        }
    }    
    
    private class LoadPhotoTask extends AsyncTask<File, Void, Bitmap> {
    	//El uso de WeakReference evita que el asyntask no permita al garbage collector
		//liberar la memoria referente al imageview para el caso de que el hilo principal se destruya
		//cuando aun el asyntask no acabo de cargar la imagen
    	private final WeakReference<ImageView> imageViewReference;
    	private File file = null; //file que se esta procesando en la asynctask    	
        
        public LoadPhotoTask (ImageView imageViewReference) {        	
         	// Use a WeakReference to ensure the ImageView can be garbage collected//        	
        	this.imageViewReference=new WeakReference<ImageView>(imageViewReference);
		}
        		        
		@Override
		protected Bitmap doInBackground(File... params) {			
			//para ejecutar en el background
			file = params[0];//asignamos el file que vamos a procesar
			
			
			//XXX: probando la rotacion automatica
			int correctionFactor = this.getPictureFactorRotation(file.getAbsolutePath());	    
			Bitmap bitmap=decodeSampledBitmapFromResource(file.getAbsolutePath(),width,height);
		    bitmap = this.rotateBitmap(bitmap,correctionFactor);					
						
			return bitmap;
		}
    
//		@Override
//		protected void onProgressUpdate(Bitmap... bitmaps) {
//			// progreso que se ca realizando
//			super.onProgressUpdate(bitmaps);
//			imageViewReference.get().setImageBitmap(bitmaps[0]);
//		}
		
		@Override
		protected void onPostExecute(Bitmap result) {
			super.onPostExecute(result);
			if (isCancelled())
				result = null;			
			//al usar el WeakReference debemos comprobar que siga existinedo el imageView
			//y no haya sido destruido por el garbage collector
			
			if (imageViewReference!=null && result!=null){
				ImageView imageview=imageViewReference.get();
				if (imageview!=null && this==getLoadPhotoTaskRunning(imageview))
					imageview.setImageBitmap(result);
			}
		}
				
	    private int getPictureFactorRotation(String picturepath){		 
			try{
		    	ExifInterface exif = new ExifInterface(picturepath);
		    	int orientation=exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);	    	
		    	switch (orientation) {
		         case ExifInterface.ORIENTATION_ROTATE_270:		        	 
		        	 return 270;	             
		         case ExifInterface.ORIENTATION_ROTATE_180:		         	 
		        	 return 180 ;//indeterminado ya que no funciona en mi movil	             
		         case ExifInterface.ORIENTATION_ROTATE_90:		        	 
		        	 return 90;	             
		          default:		        	  
		        	  return 0;
		         }    	
		    } catch (Exception e){
		    	e.printStackTrace();
		    }
			return 0;		
	    }
		
	    private Bitmap rotateBitmap(Bitmap source, int deg){
			Matrix matrix = new Matrix();
		    matrix.postRotate(deg);
		    return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
	    }
	    
    }
}


