package com.scm.TestPhoto;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;


public class PhotoFragment extends Fragment implements  OnClickListener{

	private static final int REQUEST_MINI_IMAGE = 10;
	private static final int REQUEST_IMAGE = 11;
	private static final int REQUEST_FORWARD = 12;
	private static final String JPEG_FILE_SUFFIX =  ".jpg";
	private Uri imagetempuri;
	private PhotoAction activity_callback;
	
	public interface PhotoAction{		
		public boolean isExternalRequest();
		public Uri getBackUri();
		public void finishAction(int resultCode,Intent data);		
	}
	
	@Override
	public void onAttach(Activity activity) {	
		super.onAttach(activity);
		try {
			activity_callback= (PhotoAction) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement PhotoFragment.PhotoAction interface");
        }
	}
	
	@Override
	public void onStart() {
		super.onStart();		
		Button bntTakePic = (Button) getActivity().findViewById(R.id.bntTakePic);
		Button bntTakePic2 = (Button) getActivity().findViewById(R.id.bntTakePic2);
		bntTakePic.setOnClickListener(this);
		bntTakePic2.setOnClickListener(this);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		//FIXME 
//		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
//			RelativeLayout layout = (RelativeLayout) getActivity().findViewById(R.layout.photo_fragmet);
//			if (getActivity().findViewById(R.layout.clock)==null)
//			layout.addView(getActivity().findViewById(R.layout.clock));		
//		}
		
		View view=inflater.inflate(R.layout.photo_fragmet, container,false);	
		return view;
	}
	
	//Elegimos el boton que fue pulsado
	public void onClick(View v){
		//comprobamos si la activity fue lanzada por peticion de otra aplicacion
		if (activity_callback.isExternalRequest()){
			this.fordwardIntentPhoto(activity_callback.getBackUri());
			return;	
		}		
		switch (v.getId()) {
			case R.id.bntTakePic:
				this.delegateTakeMiniPhoto();			
				break;
			case R.id.bntTakePic2:
				//si no podemos acceder al almacenamiento externo,
				//tomamos una miniphoto (no guarda ningun dato)
				if (isExternalStorageWritable())
					this.delegateTakePhoto();
				else
					this.delegateTakeMiniPhoto();
				break;		
		}		
					
	}
	
	private void delegateTakePhoto() {
		delegateTakePhoto(null);
	}
	
	private void delegateTakePhoto(Uri uri) {
		Uri imagefile;
		imagefile=createEmptyImageFile();
		if (imagefile==null){
			//fallo creando el fichero temporal
			Context context = getActivity().getBaseContext();	
			CharSequence text = context.getText(R.string.fail_tempfile);				
			Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
			return;
		}
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);		
		intent.putExtra(MediaStore.EXTRA_OUTPUT,imagefile);
		this.imagetempuri=imagefile;		
		startActivityForResult(Intent.createChooser(intent, getActivity()
				.getBaseContext().getString(R.string.chooser_title)),REQUEST_IMAGE);
	}

	private void delegateTakeMiniPhoto() {	
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		startActivityForResult(Intent.createChooser(intent, getActivity()
				.getBaseContext().getString(R.string.chooser_title)),REQUEST_MINI_IMAGE);	
	}
	
	private void fordwardIntentPhoto(Uri photo){
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		if (photo!=null)			
			intent.putExtra(MediaStore.EXTRA_OUTPUT,photo);	
		startActivityForResult(Intent.createChooser(intent, getActivity()
				.getBaseContext().getString(R.string.chooser_title)),REQUEST_FORWARD);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		Context context = getActivity().getBaseContext();		
		switch (requestCode) {
		case REQUEST_MINI_IMAGE:
			if (resultCode == Activity.RESULT_OK){
				
				Bundle bundle = data.getExtras();
				Bitmap minipic = (Bitmap) bundle.get("data");
				
				int duration = Toast.LENGTH_LONG;				
				ImageView view = new ImageView(context);
				view.setImageBitmap(minipic);				
				Toast t=Toast.makeText(context,null, duration);
				t.setGravity(Gravity.TOP, 0, 20);
				t.setView(view);
				t.show();					
			} else {
				CharSequence text = context.getText(R.string.op_cancel);				
				Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
			}			
			break;
		case REQUEST_IMAGE:
			if (resultCode == Activity.RESULT_OK){				
				String text = context.getString(R.string.img_saved);
				//no se porque pero algunos dispositivos fallan con getData()
				if (data==null||data.getData()==null){
					Toast.makeText(context, text+context.getString(R.string.unknown),
							Toast.LENGTH_SHORT).show();
				}
				else
					Toast.makeText(context, text+data.getData(), Toast.LENGTH_SHORT).show();
				galleryAddPic();
				
			} else {
				CharSequence text = context.getText(R.string.op_cancel);				
				Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
				File tempfile=new File(imagetempuri.getPath());
				tempfile.delete();
			}				
			break;
		case REQUEST_FORWARD:
			if (resultCode == Activity.RESULT_OK)
				activity_callback.finishAction(Activity.RESULT_OK,data);			
			else
				activity_callback.finishAction(Activity.RESULT_CANCELED,null);
			break;
		default:				
			break;
		}
	}
	
	//comprobamos si existe alacenamiento externo y podemos leer y escribir en el
	private boolean isExternalStorageWritable(){
		String state = Environment.getExternalStorageState();
		return Environment.MEDIA_MOUNTED.equals(state);
	}

	//a�adimos las imagenes a la gallery del sistema
	private void galleryAddPic() {
		if (this.imagetempuri!=null){
		    Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);		     
		    Uri contentUri = imagetempuri;
		    intent.setData(contentUri);
		    getActivity().sendBroadcast(intent);
		}
	}
	
	private Uri createEmptyImageFile() {
		// Create an image file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		String imageFileName = "PIC"+timeStamp+"_";		
		File aux = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);//obtenemos el file que representa la carpeta PICTURES		
		File album = new File(aux,getActivity().getString(R.string.album_name));		
		album.mkdir();//creamos la carpeta del album, si esta ya existe devolver� false		
		if(album.isDirectory()){//comprobamos si se ha creado correctamente
			File image;
			try {
				//creamos el archivo temporal(vacio)
				image = File.createTempFile(imageFileName, JPEG_FILE_SUFFIX, album);
			} catch (IOException e) {				
				e.printStackTrace();
				Log.e("ERROR", "Error creando tempFile",e);
				return null;
			}		
			return Uri.fromFile(image);
		}
		else{
			Log.e("ERROR", "Error creando directorio del album");
			return null;
		}
	}
}


