package com.scm.TestPhoto;


import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.ActionBar.TabListener;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class MainActivity extends FragmentActivity implements PhotoFragment.PhotoAction{

	 AppSectionsPagerAdapter mAppSectionsPagerAdapter ;

	    /**
	     * The {@link ViewPager} that will display the three primary sections of the app, one at a
	     * time.
	     */
	private ViewPager mViewPager;
	private ActionBar actionBar;	
	
	/*Para la gestion de las peticiones externas*/
	private boolean external_request = false;
	private Uri getback_uri = null;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        /*Gestion de petici�n de aciones*/
        Intent god = this.getIntent();        
        if (god.getAction()==MediaStore.ACTION_IMAGE_CAPTURE){
        	external_request=true;  
        	Bundle bund=god.getExtras();        	
        	if (bund!=null){
        		Uri uri=bund.getParcelable(MediaStore.EXTRA_OUTPUT);
        		if(uri!=null){
        			//almacenamos la petici�n para traspasar a la camara
        			getback_uri=uri;        			
        		}
        	}        		      		
        }
        
        //Create the adapter that will return a fragment for each of the three primary sections
        // of the app.
       mAppSectionsPagerAdapter = new AppSectionsPagerAdapter(getSupportFragmentManager(),getBaseContext());
       
       // Set up the action bar.
       actionBar = getActionBar();
       actionBar.setHomeButtonEnabled(false);
       actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
       
       // Set up the ViewPager, attaching the adapter and setting up a listener for when the
       // user swipes between sections.
       mViewPager = (ViewPager) findViewById(R.id.pager);
       mViewPager.setAdapter(mAppSectionsPagerAdapter);
       mViewPager.setOnPageChangeListener(new CustomSimpleOnPageChangeListener());
       
       CustomTabListener customtablistener = new CustomTabListener();
       // For each of the sections in the app, add a tab to the action bar.
       for (int i = 0; i < mAppSectionsPagerAdapter.getCount(); i++) {
           // Create a tab with text corresponding to the page title defined by the adapter.
           // Also specify the object that implements TabListener interface, as the
           // listener for when this tab is selected.
    	   ActionBar.Tab tab=actionBar.newTab();
    	   tab.setText(mAppSectionsPagerAdapter.getPageTitle(i));
    	   tab.setTabListener(customtablistener);
           actionBar.addTab(tab);
       }       
    }

    //EXCEDENT
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
	
    @Override
	public boolean isExternalRequest() {		
		return this.external_request;
	}

	@Override
	public Uri getBackUri() {		
		return this.getback_uri;
	}

	@Override
	public void finishAction(int resultCode, Intent data) {		        	
    	this.setResult(RESULT_OK, data);
    	this.finish();
	}
    
	public static class AppSectionsPagerAdapter extends FragmentPagerAdapter {
		
		final int totalsections; 
		final String[] tabTitles;
		
	    public AppSectionsPagerAdapter(FragmentManager fm, Context cntx) {
	        super(fm);
	        tabTitles=cntx.getResources().getStringArray(R.array.tab_titles);
	        totalsections=tabTitles.length;
	    }

	    @Override
	    public Fragment getItem(int i) {
	        switch (i) {
	            case 0:
	                // The first section of the app is the most interesting -- it offers
	                // a launchpad into the other demonstrations in this example application.
	                return new PhotoFragment();
	            
	            case 1:
	            	//The second section of the app, the gallery (gridview)
	            	return new GalleryViewFragment();
	            	
	            default:
	                // The other sections of the app are dummy placeholders.
//	                Fragment fragment = new DummySectionFragment();
//	                
//	                Bundle args = new Bundle();
//	                args.putInt(DummySectionFragment.ARG_SECTION_NUMBER, i + 1);
//	                fragment.setArguments(args);
//	                return fragment;
	            	return new ListViewFragment();
	        }
	    }

	    @Override
	    public int getCount() {
	        return totalsections;
	    }

	    @Override
	    public CharSequence getPageTitle(int position) {	    	
	        return this.tabTitles[position];	    	
	    }
	}
	
	public final class CustomTabListener implements TabListener{

		@Override
		public void onTabReselected(Tab tab, FragmentTransaction ft) {
			// Se genera cuando se vuelve a pulsar sobre una tab ya con foco
//			Log.d("TAGLISTENER", "TabReselected2");
		}

		@Override
		public void onTabSelected(Tab tab, FragmentTransaction ft) {
			// Se genera cuando se pasa el foco a una nueva tab
//			Log.d("TAGLISTENER", "TabSelected2");
			mViewPager.setCurrentItem(tab.getPosition());			
		}

		@Override
		public void onTabUnselected(Tab tab, FragmentTransaction ft) {
			// Se genera cuando una tab pierde el foco
//			Log.d("TAGLISTENER", "TabUnselected2");		
		}
	}
	
	public final class CustomSimpleOnPageChangeListener extends ViewPager.SimpleOnPageChangeListener{
		@Override
		public void onPageSelected(int position) {
			// When swiping between different app sections, select the corresponding tab.
            // We can also use ActionBar.Tab#select() to do this if we have a reference to the
            // Tab.			
			actionBar.setSelectedNavigationItem(position);						
		}
	}
	
	
	//EXCEDENT
	 public static class DummySectionFragment extends Fragment {

	        public static final String ARG_SECTION_NUMBER = "section_number";

	        @Override
	        public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                Bundle savedInstanceState) {
	        	
//	            View rootView = inflater.inflate(R.layout.fragment_section_dummy, container, false);
	            Bundle args = getArguments();
//	            ((TextView) rootView.findViewById(android.R.id.text1)).setText(
//	                    "Dummy section num "+args.getInt(ARG_SECTION_NUMBER));	            
//	            TextView rootView = new TextView(container.getContext());
	            TextView rootView = new TextView(getActivity().getApplicationContext());
	            rootView.setText("Dummy section num "+args.getInt(ARG_SECTION_NUMBER));
	            rootView.setTextColor(Color.MAGENTA);
	            rootView.setBackgroundColor(Color.WHITE);
	            rootView.setTextSize(25);	            
	            return rootView;
	        }
	    }

	
	 //Los metodos definidos en el xml ligados a botones, solo funcionan 
	 //invocandolos en las activities, no importa que el boton estea en 
//	 public void onClick(View v){
//			Context context = getApplicationContext();
//			CharSequence text = "Hello toast!";
//			int duration = Toast.LENGTH_SHORT;
//			Toast.makeText(context, text, duration).show();	
//		}
}


