package com.scm.TestPhoto;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.AdapterView.OnItemClickListener;

public class GalleryViewFragment extends Fragment {
	
	private GridView grid=null;
	private ImageAdapter adapter=null;

	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {		
		
		View view=inflater.inflate(R.layout.gallery_fragment, container, false);
			
		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {	
		super.onActivityCreated(savedInstanceState);
		this.grid=(GridView) getActivity().findViewById(R.id.gallery_gridview);		
		adapter=new ImageAdapter(getActivity().getBaseContext());		
		grid.setAdapter(adapter);
		grid.setOnItemClickListener(new OnItemClickListener() {
        	@Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                //Toast.makeText(getActivity().getBaseContext(), "" + position, Toast.LENGTH_SHORT).show();//EXCEDENT
                Intent intent= new Intent(getActivity(),FullscreenActivity.class);
                intent.putExtra(FullscreenActivity.POSITION, position);
                startActivity(intent);
            }
        });
	}
	
	@Override
	public void onStart() {	
		super.onStart();
		adapter.notifyDataSetChanged();
		//grid.setAdapter(adapter);
	}
}
