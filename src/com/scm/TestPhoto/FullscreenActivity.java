package com.scm.TestPhoto;



import java.io.File;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.media.ExifInterface;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

/**
 * 
 * @author Domin
 *
 */
public class FullscreenActivity extends Activity {
	public static String POSITION="Position";
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen);
        
        ImageView image = (ImageView) findViewById(R.id.fullimageView);
        
        Intent god=this.getIntent();
        int position=god.getIntExtra(POSITION,-1);
                
        if (isExternalStorageWritable()){
        	File file=retryFile(position);
        	
			Context context = getApplicationContext();        		
			CharSequence text = "PIC-"+String.valueOf(position)+"::"+file.getAbsolutePath();
			int duration = Toast.LENGTH_LONG;
			Toast.makeText(context, text, duration).show();
									
        	Bitmap bm=this.sampleDecodeHandler(file.getAbsolutePath());        	

        	image.setImageBitmap(bm);
        	Log.d("Check Full Screen", image==null?"ImageView null":"ImageView not null");
        	Log.d("Check Full Screen", image==null?"Bitmap null":"Bitmap not null");
        }  
        this.fullScreenMode();
    }
      
    //comprobamos si existe alacenamiento externo y podemos leer y escribir en el
	private boolean isExternalStorageWritable(){
		String state = Environment.getExternalStorageState();
		return Environment.MEDIA_MOUNTED.equals(state);
	}
	
	private File retryFile(int position){
		File aux = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);		
		File album = new File(aux,this.getString(R.string.album_name));
		if (album.isDirectory() && album.canRead()){
			File [] files=album.listFiles();
			if (position>=0 && position<files.length)
				return files[position];			
		}	
		return null;
	}
	
@SuppressLint("InlinedApi") private void fullScreenMode(){		
		int uiOptions = this.getWindow().getDecorView().getSystemUiVisibility();
        int newUiOptions = uiOptions;
//		int newUiOptions=0;
        
        if (Build.VERSION.SDK_INT >= 11){
	        // Hide action bar
	        getActionBar().hide(); 
        }
        
        // If the Android version is lower than Jellybean, use this call to hide
        // the status bar.  
        if (Build.VERSION.SDK_INT < 16) {
        	getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, 
        			WindowManager.LayoutParams.FLAG_FULLSCREEN);
	    }
       
		// Navigation bar hiding:  Backwards compatible to ICS.
        if (Build.VERSION.SDK_INT >= 14)
            newUiOptions |= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        
        // Status bar hiding: Backwards compatible to Jellybean
        if (Build.VERSION.SDK_INT >= 16){
            newUiOptions |= View.SYSTEM_UI_FLAG_FULLSCREEN;
            newUiOptions |= View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;                      
        }
        
        if (Build.VERSION.SDK_INT >= 18) 
        	newUiOptions |= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

        this.getWindow().getDecorView().setSystemUiVisibility(newUiOptions);        
	}

	private Bitmap sampleDecodeHandler(String mCurrentPhotoPath) {        
	    WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
	    Display display = wm.getDefaultDisplay();
	    Point size=new Point();
	    display.getSize(size);
	    int targetW = size.x; 
	    int targetH = size.y;
	    
	    int correctionFactor = this.getPictureFactorRotation(mCurrentPhotoPath);	    
	    //Bitmap bitmap = decodeSampledBitmapFromResource2(mCurrentPhotoPath,targetW,targetH);
	    Bitmap bitmap = decodeSampledBitmapFromResource(mCurrentPhotoPath,targetW,targetH);
	    bitmap = this.rotateBitmap(bitmap,correctionFactor);
	    
	    return bitmap;
	}

	
	private static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
	    // Raw height and width of image
	    final int height = options.outHeight;
	    final int width = options.outWidth;
	    int inSampleSize = 1;
	
	    if (height > reqHeight || width > reqWidth) {
	
	        final int halfHeight = height / 2;
	        final int halfWidth = width / 2;
	
	        // Calculate the largest inSampleSize value that is a power of 2 and keeps both
	        // height and width larger than the requested height and width.
	        while ((halfHeight / inSampleSize) > reqHeight
	                && (halfWidth / inSampleSize) > reqWidth) {
	            inSampleSize *= 2;
	        }
	    }
	    return inSampleSize;
    }
    
    
	private static Bitmap decodeSampledBitmapFromResource(String pathName,
            final int targetWidth, final int targetHeight) {
    	// Check input values
    	if (targetWidth==0 || targetHeight==0){
    		throw new RuntimeException(){
				private static final long serialVersionUID = 1L;
			@Override
    		public String getMessage() {    			
    			return "BadInputValuesException: targetWidth="+String.valueOf(targetWidth)
    					+" targetHeight="+String.valueOf(targetHeight)+"\n"
    					+super.getMessage();
    		}};
    	}

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true; //selecionamos la decodificacion que solo calcula los parametros de la imagen en la salidas de options
        BitmapFactory.decodeFile(pathName, options);  //obtenemos lo parametros de la imagen en options

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, targetWidth, targetHeight); //calculamos el subsamplig para la imagen (reducion de tama�o)
      
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;//selecionamos la decodificacion normal
        options.inPurgeable = true;//se puede liberar la memoria del imagen //XXX
        return BitmapFactory.decodeFile(pathName, options); //obtenemos la imagen decodificada en un bitmap segun las opcione de entrada que establecimos
    }
	
	
	//EXCEDENT: non o estou usando ahora
	private Bitmap decodeSampledBitmapFromResource2(final String mCurrentPhotoPath,final int targetWidth,final int targetHeight) {
		// Check input values
		if (targetWidth==0 || targetHeight==0){
			throw new RuntimeException(){
				private static final long serialVersionUID = 1L;
			@Override
			public String getMessage() {    			
				return "BadInputValuesException: targetWidth="+String.valueOf(targetWidth)
						+" targetHeight="+String.valueOf(targetHeight)+"\n"
						+super.getMessage();
			}};
		}
		
		// Get the dimensions of the bitmap
	    BitmapFactory.Options bmOptions = new BitmapFactory.Options();
	    bmOptions.inJustDecodeBounds = true;
	    
	    BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
	    int photoW = bmOptions.outWidth;
	    int photoH = bmOptions.outHeight;
	    
	    // Determine how much to scale down the image
	    int scaleFactor = Math.min(photoW/targetWidth, photoH/targetHeight);
	
	    // Decode the image file into a Bitmap sized to fill the View
	    bmOptions.inJustDecodeBounds = false;
	    bmOptions.inSampleSize = scaleFactor;
	    bmOptions.inPurgeable = true;   
	    
	    Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
	    return bitmap;
	}
	
	private int getPictureFactorRotation(String picturepath){		 
		try{
	    	ExifInterface exif = new ExifInterface(picturepath);
	    	int orientation=exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);	    	
	    	switch (orientation) {
		    	case ExifInterface.ORIENTATION_ROTATE_270:		        	 
		        	 return 270;	             
		        case ExifInterface.ORIENTATION_ROTATE_180:		         	 
		        	 return 180 ;//indeterminado ya que no funciona en mi movil	             
		        case ExifInterface.ORIENTATION_ROTATE_90:		        	 
		        	 return 90;	             
		         default:		        	  
		        	  return 0;
	         }    	
	    } catch (Exception e){
	    	e.printStackTrace();
	    }
		return 90;		
	}
	
	private Bitmap rotateBitmap(Bitmap source, int deg){
		Matrix matrix = new Matrix();
	    matrix.postRotate(deg);
	    return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
	}
	
}

