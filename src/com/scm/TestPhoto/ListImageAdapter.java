package com.scm.TestPhoto;

import java.io.File;
import java.io.FilenameFilter;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ListImageAdapter extends BaseAdapter {
    private final Context context;
    @SuppressLint("SimpleDateFormat") final static SimpleDateFormat dateformat=new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss");
    private File album;
    private File[] pics;
    
    /* Imageviews dimension*/
    private final int width;
    private final int height;
    
    private final ActivityLauncher launcher;
    public interface ActivityLauncher{
    	void launchActivity(Intent intent);
    }
    
    public ListImageAdapter(Context c,ActivityLauncher launcher) {    	
    	context = c;
    	final DisplayMetrics metrics=context.getResources().getDisplayMetrics();
    	width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 160, metrics);
        height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 120, metrics);
        this.launcher=launcher;
	}   
    
 // comprueba si el medio esta montado y existe el directorio del album
    private boolean canreadImages(){
    	String state = Environment.getExternalStorageState();
		if(Environment.MEDIA_MOUNTED.equals(state)){			
			File aux = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);		
			File album = new File(aux,context.getString(R.string.album_name));
			if (album.isDirectory() && album.canRead()){
				this.album=album;
				return true;
			}						
		}
		this.album=null;
		return false;
    }
    
	public int getCount() {
		// si se pueden leer las imagenes devolvemos el numero que haya
		if (canreadImages()){			
			FilenameFilter filter= new FilenameFilter() {
			    public boolean accept(File dir, String name){
			        return (name.endsWith(".jpg")||name.endsWith(".jpeg")); 
			    }
			};			
			this.pics=album.listFiles(filter);
			Log.d("IMAGELIST","getCount="+String.valueOf(pics.length));
			int i=0;
			for (File pic:pics)
				Log.d("IMAGELIST","PIC"+(i++)+" "+pic.getName());
	        return pics.length;
		}
		//si no se pueden leer, asumimos que no hay ninguna
		return 0;
    }
    
    // no lo vamos a usar asi que lo dejamos vac�o
    public Object getItem(int position) {    	
        return pics[position];
    }
    
    @Override
	public long getItemId(int position) {	
		return position;
	}
    
    public static class ViewHolder{
    	ImageView image;
    	TextView  name;
    	TextView  date;
    	int position;
    	WeakReference<LoadPhotoTask> loadPhotoTaskReference;
    }
    
    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {    
        Log.d("IMAGELIST", "in getView for position " + position + ", convertView is "
                + ((convertView == null) ? "null" : "being recycled"));
        Log.d("IMAGELIST",parent==null?"parent null":"parent not null");
        final ViewHolder holder;
        if (convertView == null) {  // if it's not recycled, initialize some attributes        	
        	//inflate the layout       	
        	LayoutInflater inflater=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);        	
        	convertView = inflater.inflate(R.layout.row_list, parent, false);
        	
        	//set up the ViewHolder        	
        	holder = new ViewHolder(); 
        	holder.image=(ImageView) convertView.findViewById(R.id.image);
			holder.name=(TextView) convertView.findViewById(R.id.file_name);			
			holder.date=(TextView) convertView.findViewById(R.id.created_date);
			holder.position=position;		
			
			holder.image.setOnClickListener(new OnClickListener(){
				@Override
				public void onClick(View arg0) {					
					Intent intent= new Intent(context,FullscreenActivity.class);
	                intent.putExtra(FullscreenActivity.POSITION,holder.position);
	                launcher.launchActivity(intent);
				}});
			
			//store the holder with the views
			convertView.setTag(holder);            
        } else {
        	holder = (ViewHolder) convertView.getTag();
        	holder.position=position;
        }               
        loadImage(holder);        
        return convertView;
    }
            
    private void loadImage(ViewHolder holder){
    	Log.d("IMAGELIST","entrando en loadImage position: "+holder.position+" imageView: "+holder.image.hashCode());
    	
    	if (cancelPotentialWork(pics[holder.position],holder)){	    	
	    	final LoadPhotoTask asyncload = new LoadPhotoTask(holder);
	    	final Bitmap placeholder = BitmapFactory.decodeResource(context.getResources(), R.drawable.placeholder);
	    	
	    	holder.loadPhotoTaskReference=new WeakReference<LoadPhotoTask>(asyncload);	    	
	//    	asyncload.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, pics[holder.position]);    	
	    	asyncload.execute(pics[holder.position]);
	    	holder.image.setImageBitmap(placeholder);//selecionamos una imagen temporal
    	}
    }
    
    public static boolean cancelPotentialWork(File file, ViewHolder holder) {    	
    	final LoadPhotoTask loadPhotoTask=(holder.loadPhotoTaskReference!=null)?holder.loadPhotoTaskReference.get():null; 
        //Log.d("IMAGELIST","entrando en cancelPotentialWork imageView: "+imageView.hashCode()); //EXCEDENT
        if (loadPhotoTask != null) {
            final File taskFile = loadPhotoTask.file;
            // Si el taskFile de la asynctask no esta asignado o difiere del nuevo file
            if (taskFile == null || !taskFile.equals(file)) {
                // Cancelar la tarea previa
            	loadPhotoTask.cancel(true);
            } else {
            	// El trabajo en progreso es el mismo
                return false;
            }
        }
        // No hay tareas asociadas con el ImageView o la tarea existente fue cancelada
        return true;
    }    
    
    /*Funciones para la Carga eficiente de los bitmaps*/
	private static Bitmap decodeSampledBitmapFromResource2(final String mCurrentPhotoPath,final int targetWidth,final int targetHeight) {
    	// Check input values
    	if (targetWidth==0 || targetHeight==0){
    		throw new RuntimeException(){
				private static final long serialVersionUID = 1L;
			@Override
    		public String getMessage() {    			
    			return "BadInputValuesException: targetWidth="+String.valueOf(targetWidth)
    					+" targetHeight="+String.valueOf(targetHeight)+"\n"
    					+super.getMessage();
    		}};
    	}
    	
    	// Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;
        
        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/targetWidth, photoH/targetHeight);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;   
        
        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        return bitmap;
    }    
    
   /**
    * Clase para guardar la refecencia al imageView que se esta cargando dentro de una Asyntask
    * LoadPhotoTask para 
    * @author Domin
    *
    */       
    private class LoadPhotoTask extends AsyncTask<File, Void, Bitmap> {
    	//El uso de WeakReference evita que el asyntask no permita al garbage collector
		//liberar la memoria referente al imageview para el caso de que el hilo principal se destruya
		//cuando aun el asyntask no acabo de cargar la imagen
    	private final WeakReference<ViewHolder> holderReference;
    	private File file = null; //file que se esta procesando en la asynctask    	
        
        public LoadPhotoTask(ViewHolder holder) {
        	Log.d("IMAGELIST","Creando nueva task, id:"+holder.hashCode());
         	// Use a WeakReference to ensure the ImageView can be garbage collected
//        	
        	this.holderReference=new WeakReference<ViewHolder>(holder);
		}
        		        
		@Override
		protected Bitmap doInBackground(File... params) {			
			//para ejecutar en el background
			file = params[0];//asignamos el file que vamos a procesar				
			Log.d("IMAGELIST","cargando bitmap, file:"+file.getAbsolutePath()+" en imageview id: "+holderReference.get().hashCode()); //XXX
			Log.d("IMAGELIST","cargando bitmap, dimensiones width: "+
					String.valueOf(holderReference.get().image.getWidth())+" height: "
					+String.valueOf(holderReference.get().image.getHeight())); //XXX
			//Bitmap bitmap=decodeSampledBitmapFromResource2(file.getAbsolutePath(),width,height);
			Bitmap bitmap;
			if (holderReference.get().image.getWidth()!=0 && holderReference.get().image.getHeight()!=0){
				bitmap=decodeSampledBitmapFromResource2(file.getAbsolutePath(),
						holderReference.get().image.getWidth(),
						holderReference.get().image.getHeight());
			}
			else{
				bitmap=decodeSampledBitmapFromResource2(file.getAbsolutePath(),width,height);
			}
				
			return bitmap;
		}   

		
		@Override
		protected void onPostExecute(Bitmap result) {
			super.onPostExecute(result);
			if (isCancelled())
				result = null;			
			//al usar el WeakReference debemos comprobar que siga existinedo el imageView
			//y no haya sido destruido por el garbage collector			
			if (holderReference!=null && result!=null){
				ViewHolder holder=holderReference.get();
				ImageView imageview=holder.image;				
				if (imageview!=null && this==holder.loadPhotoTaskReference.get())
					imageview.setImageBitmap(result);					
					holder.name.setText(file.getName());				
					Date date=new Date(file.lastModified());
					holder.date.setText(ListImageAdapter.dateformat.format(date));				
			}
		}
    }	
}


